import { Router } from 'express';

const router: Router = Router();

router.get('/', async (req, res) => {
  try {
    res.json({
        status: 'running',
        nodeEnv: process.env.NODE_ENV,
        hostname: process.env.HOSTNAME,
        higherLifeForm: 'mouse',
        answer: 42
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
});

export const StatusController: Router = router;
