import * as dotenv from 'dotenv-extended';

import { AppServer } from './server';

dotenv.load();

const server = new AppServer();
const app = server.getApp();
export { app, server };
