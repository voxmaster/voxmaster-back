import * as path from 'path';
import { Model, Op } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';

const sequelize = new Sequelize({
    database: process.env.DATABASE_NAME,
    dialect: 'postgres',
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS,
    host: process.env.DATABASE_HOST,
    modelPaths: [__dirname + '/models'],
    operatorsAliases: { $like: Op.iLike },
    logging: true
});

export const dbConnect = async () =>
  new Promise<any>((resolve, rej) => {
    sequelize
        .sync() // { force: true }
        .then(res => {
            console.log('Database sync');
            resolve();
        })
        .catch(err => {
            console.error(err);
            rej(err);
        });
  });

export const getSeqlize = () =>
  sequelize;

// export const getModel = (model: string) => {
//   const modelName = model.charAt(0).toUpperCase() + model.slice(1);
//   const supplierModel: any = sequelize.modelManager.getModel(modelName);
//   return supplierModel;
// };
